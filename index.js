'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3977;

mongoose.connect('mongodb://localhost:27017/monkeyfy', (err, resp) => {
    if(err){
        throw err;
    }else{
        console.log('la base de datos esta corriendo correctamente...');

        app.listen(port, function(){
            console.log("servidor del API Rest escuchando en http://localhost:"+port);
        });
    }
});