'use strict'

//cargamos express para acceder al sistemas de rutas
var express = require('express');
//Cargamos el controlador de artista
var AlbumController = require('../controllers/album');
//usamos Router de express, nos permite dar uso a las funciones GET, PUT, POST...
var api = express.Router();
//Cargamos el middleware que nos permitirá restringir el acceso a solamente usuarios logueados a los metodos de el controlador
var md_auth = require('../middlewares/authenticated');
//permite suber fichero y enviar fichero por el protocolo http
var multiparty = require('connect-multiparty');
var md_upload = multiparty({ uploadDir: './uploads/albums'});

api.get('/album/:id', md_auth.ensureAuth, AlbumController.getAlbum);
api.post('/album-register', md_auth.ensureAuth, AlbumController.saveAlbum);
api.get('/albums/:artist?', md_auth.ensureAuth, AlbumController.getAlbums);
api.put('/album-update/:id', md_auth.ensureAuth, AlbumController.updateAlbum);
api.delete('/album-delete/:id', md_auth.ensureAuth, AlbumController.deleteAlbum);
api.post('/upload-image-album/:id', [md_auth.ensureAuth, md_upload], AlbumController.uploadImage);
api.get('/get-image-album/:imageFile', AlbumController.getImageFile);

//exportamos api

module.exports = api;

//por ultimo hay que cargar el fichero de rutas en nuestra app principal (app.js)