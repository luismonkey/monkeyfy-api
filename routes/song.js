'use strict'

//cargamos express para acceder al sistemas de rutas
var express = require('express');
//Cargamos el controlador de artista
var SongController = require('../controllers/song');
//usamos Router de express, nos permite dar uso a las funciones GET, PUT, POST...
var api = express.Router();
//Cargamos el middleware que nos permitirá restringir el acceso a solamente usuarios logueados a los metodos de el controlador
var md_auth = require('../middlewares/authenticated');
//permite suber fichero y enviar fichero por el protocolo http
var multiparty = require('connect-multiparty');
var md_upload = multiparty({ uploadDir: './uploads/songs'});


api.get('/song/:id', md_auth.ensureAuth, SongController.getSong);
api.post('/song-register', md_auth.ensureAuth, SongController.saveSong);
api.get('/songs/:album?', md_auth.ensureAuth, SongController.getSongs);
api.put('/song-update/:id', md_auth.ensureAuth, SongController.updateSong);
api.delete('/song-delete/:id', md_auth.ensureAuth, SongController.deleteSong);
api.post('/upload-file/:id', [md_auth.ensureAuth, md_upload], SongController.uploadFile);
api.get('/get-file/:songFile', SongController.getSongFile);

module.exports = api;