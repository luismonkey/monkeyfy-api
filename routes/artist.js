'use strict'

//cargamos express para acceder al sistemas de rutas
var express = require('express');
//Cargamos el controlador de artista
var ArtistController = require('../controllers/artist');
//usamos Router de express, nos permite dar uso a las funciones GET, PUT, POST...
var api = express.Router();
//Cargamos el middleware que nos permitirá restringir el acceso a solamente usuarios logueados a los metodos de el controlador
var md_auth = require('../middlewares/authenticated');
//permite suber fichero y enviar fichero por el protocolo http
var multiparty = require('connect-multiparty');
var md_upload = multiparty({ uploadDir: './uploads/artists'});

api.get('/artist/:id', md_auth.ensureAuth, ArtistController.getArtist);
api.post('/artist-register', md_auth.ensureAuth, ArtistController.saveArtist);
api.get('/artists/:page?', md_auth.ensureAuth, ArtistController.getArtists);
api.put('/artist-update/:id', md_auth.ensureAuth, ArtistController.updateArtist);
api.delete('/artist-delete/:id', md_auth.ensureAuth, ArtistController.deleteArtist);
api.post('/upload-image-artist/:id', [md_auth.ensureAuth, md_upload], ArtistController.uploadImage);
api.get('/get-image-artist/:imageFile', ArtistController.getImageFile);

//exportamos api

module.exports = api;

//por ultimo hay que cargar el fichero de rutas en nuestra app principal (app.js)