'use strict'

//importamos los modulos para manejar el sistema de fichero

var path = require('path');
var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');

//Importamos los modelos a utilizar

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

//Buscar un artista especifico
function getArtist(req, res){
    var artistId = req.params.id;

    Artist.findById(artistId, (err, artist) => {
        if(err){
            res.status(500).send({menssage: 'Error al buscar el artista'});
        }else{
            if(!artist){
                res.status(404).send({menssage: 'El artista no existe'});
            }else{
                res.status(200).send({artist});
            }
        }
    });
}

//Registrar un artista  
function saveArtist(req, res){
    var artist = new Artist();

    var params = req.body;
    artist.name = params.name;
    artist.description = params.description;
    artist.image = 'null';

    artist.save((err, artistStored) => {
        if(err){
            res.status(500).send({menssage: 'Error al guardar artista'});
        }else{
            if(!artistStored){
                res.status(404).send({menssage: 'No se ha podido guardar al artista'});
            }else{
                res.status(200).send({artist: artistStored});
            }
        }
    });
}

//Buscar todos los artistas registrados
function getArtists(req, res){
    //usado para paginar la lista
    if(req.params.page){
        var page = req.params.page;
    }else{
        var page = 1;
    }
    //para enumerar la cantidad maxima de artistas por página
    var itemsPerPage = 3;

    Artist.find().sort('name').paginate(page, itemsPerPage, (err, artists, total) => {
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!artists){
                res.status(404).send({menssage: 'No hay artistas'});
            }else{
                return res.status(200).send({
                    total_items: total,
                    artists: artists
                });
            }
        }
    });
}

//Actualizar un artista en especifico
function updateArtist(req, res){
    var artistId = req.params.id;
    var update = req.body;

    Artist.findByIdAndUpdate(artistId, update, (err, artistUpdated) => {
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!artistUpdated){
                res.status(404).send({menssage: 'El artista no ha sido actualizado'});
            }else{
                res.status(200).send({artist: artistUpdated});
            }
        }
    });
}

//Eliminar un artista
function deleteArtist(req, res){
    var artistId = req.params.id;
    
    Artist.findByIdAndRemove(artistId, (err, artistRemoved) => {
        console.log('Artista: '+artistRemoved);
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!artistRemoved){
                res.status(404).send({menssage: 'El artista no ha sido eliminado'});
            }else{
                //Buscamos y eliminamos los albums asociados al artista eliminado
                Album.find({artist: artistRemoved._id}).remove((err, albumRemoved) => {
                    if(err){
                        res.status(500).send({menssage: 'Error al eliminar el album(s)'});
                    }else{
                        if(!albumRemoved){
                            res.status(404).send({menssage: 'El album asociado al artista no ha sido eliminado'});
                        }else{
                            //BUscamos y eliminamos las canciones asociadas al album
                            Song.find({album: albumRemoved._id}).remove((err, songRemoved) => {
                                if(err){
                                    res.status(500).send({menssage: 'Error al eliminar cancion(es)'});
                                }else{
                                    if(!songRemoved){
                                        res.status(404).send({menssage: 'Cancion(es) asociado al album no ha(n) sido eliminada(s)'});
                                    }else{
                                        res.status(200).send({artist: artistRemoved});
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

//Subir imagen del artista
function uploadImage(req, res){
    var artistId = req.params.id;
    var file_name = "no hay imagen...";

    if(req.files){
        var file_path = req.files.image.path;
        var file_split = file_path.split('/');
        var file_name = file_split[2];
        
        
        var ext_split = file_name.split('.');
        var file_ext = ext_split[1];

        if(file_ext == 'png' || file_ext == 'jpeg' || file_ext == 'gif'){
            Artist.findByIdAndUpdate(artistId, {image: file_name}, (err, artistUpdated) => {
                    if(!artistUpdated){
                        res.status(404).send({menssage: 'NO se ha podido actualizar el artista'});
                    }else{
                        res.status(200).send({artist: artistUpdated});
                    }
            });
        }else{
            res.status(200).send({menssage: 'La extensión no es válida'});
        }
    }else{
        res.status(200).send({menssage: 'NO haz subido ninguna imagen'});
    }
}

function getImageFile(req, res){

    // se obtiene el path de la imagen que viene por la url
    var imageFile = req.params.imageFile;
    //se busca la imagen en la carpeta donde de imagen de usuarios (disco duro)
    var path_file = './uploads/artists/'+imageFile;
    //se comprueba la existencia de la imagen
    fs.exists(path_file, function(exists){
        if(exists){
            //si existe, se devuelve el path y se resuelve para mostrar la imagen en pantalla
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({menssage: 'NO existe la imagen...'})
        }

    });
}

module.exports = {
    getArtist,
    saveArtist,
    getArtists,
    updateArtist,
    deleteArtist,
    uploadImage,
    getImageFile
}