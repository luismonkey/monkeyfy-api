'use strict'

//importamos los modulos para manejar el sistema de fichero

var path = require('path');
var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');

//Importamos los modelos a utilizar

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getSong(req, res){
    var songId = req.params.id;

    Song.findById(songId).populate({path: 'album'}).exec((err, song) => {
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
            if(!song){
                res.status(404).send({message: 'La canción no existe'});
            }else{
                res.status(200).send({song});
            }
        }
    });
    
}

function getSongs(req,res){
    var albumId = req.params.album;

    if(!albumId){
        //Sacar todo los albums de la BBDD
        var find = Song.find({}).sort('number');
    }else{
        //Sacar los albums del artista en concreto
        var find = Song.find({album: albumId}).sort('number');
    }

    find.populate({
        path: 'album',
        populate: {
            path: 'artist',
            model: 'Artist'
        }
    }).exec((err, songs) => {
        if(err){
            res.status(500).send({message: 'Error en la petición'});
        }else{
            if(!songs){
                res.status(404).send({message: 'No hay canciones'});
            }else{
                res.status(200).send({songs});
            }
        }
    });

}

function saveSong(req, res){
    var song = new Song();

    var params = req.body;
    song.number = params.number;
    song.name = params.name;
    song.duration = params.duration;
    song.file = 'null';
    song.album = params.album;
    console.log(song);

    song.save((err, songStored) => {
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!songStored){
                res.status(404).send({menssage: 'No se ha podido guardar al album'});
            }else{
                res.status(200).send({song: songStored});
            }
        }
    });
}

function updateSong(req, res){
    var songId = req.params.id;
    var update = req.body;

    Song.findByIdAndUpdate(songId, update, (err, songUpdated) => {
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!songUpdated){
                res.status(404).send({menssage: 'Error al actualizar la canción'});
            }else{
                res.status(200).send({songUpdated});
            }
        }
    });
}

function deleteSong(req, res){
    var songId = req.params.id;

    Song.findByIdAndRemove(songId, (err, songRemoved) => {
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!songRemoved){
                res.status(404).send({menssage: 'Error al eliminar la canción'});
            }else{
                res.status(200).send({songRemoved});
            }
        }
    });
}

function uploadFile(req, res){
    var songId = req.params.id;
    var file_name = "no hay canción...";

    if(req.files){
        var file_path = req.files.file.path;
        var file_split = file_path.split('/');
        var file_name = file_split[2];
        
        
        var ext_split = file_name.split('.');
        var file_ext = ext_split[1];

        if(file_ext == 'mp3' || file_ext == 'ogg'){
            Song.findByIdAndUpdate(songId, {file: file_name}, (err, songUpdated) => {
                    if(!songUpdated){
                        res.status(404).send({menssage: 'No se ha podido actualizar la canción'});
                    }else{
                        res.status(200).send({song: songUpdated});
                    }
            });
        }else{
            res.status(200).send({menssage: 'La extensión no es válida'});
        }
    }else{
        res.status(200).send({menssage: 'No haz subido ninguna canción'});
    }
}

function getSongFile(req, res){

    // se obtiene el path de la imagen que viene por la url
    var songFile = req.params.songFile;
    //se busca la imagen en la carpeta donde de imagen de usuarios (disco duro)
    var path_file = './uploads/songs/'+songFile;
    //se comprueba la existencia de la imagen
    fs.exists(path_file, function(exists){
        if(exists){
            //si existe, se devuelve el path y se resuelve para mostrar la imagen en pantalla
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({menssage: 'NO existe la canción...'})
        }

    });
}
module.exports = {
    getSong,
    saveSong,
    getSongs,
    updateSong,
    deleteSong,
    uploadFile,
    getSongFile
}