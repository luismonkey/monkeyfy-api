'use strict'

//importamos los modulos para manejar el sistema de fichero

var path = require('path');
var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');

//Importamos los modelos a utilizar

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

//Buscar un artista especifico
function getAlbum(req, res){
    var albumId = req.params.id;

    Album.findById(albumId).populate({path: 'artist'}).exec((err, album) => {
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!album){
                res.status(404).send({menssage: 'El album no existe'});
            }else{
                res.status(200).send({album});
            }
        }
    });
}

function getAlbums(req, res){
    var artistId = req.params.artist;

    if(!artistId){
        //Sacar todo los albums de la BBDD
        var find = Album.find({}).sort('title');
    }else{
        //Sacar los albums del artista en concreto
        var find = Album.find({artist: artistId}).sort('year');
    }


    find.populate({path: 'artist'}).exec((err, albums) => {
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!albums){
                res.status(404).send({menssage: 'El album no existe'});
            }else{
                res.status(200).send({albums});
            }
        }
    });

}

function saveAlbum(req, res){
    var album = new Album();

    var params = req.body;
    album.title = params.title;
    album.description = params.description;
    album.year = params.year;
    album.image = 'null';
    album.artist = params.artist;

    album.save((err, albumStored) => {
        if(err){
            res.status(500).send({menssage: 'Error al guardar el album'});
        }else{
            if(!albumStored){
                res.status(404).send({menssage: 'No se ha podido guardar al album'});
            }else{
                res.status(200).send({album: albumStored});
            }
        }
    });
}

function updateAlbum(req, res){
    var albumId = req.params.id;
    var update = req.body;

    Album.findByIdAndUpdate(albumId,update, (err, albumUpdated) => {
        if(err){
            res.status(500).send({menssage: 'Error en la petición'});
        }else{
            if(!albumUpdated){
                res.status(404).send({menssage: 'Error al actualizar el album'});
            }else{
                res.status(200).send({album: albumUpdated});
            }
        }
    });
}

function deleteAlbum(req, res){
    var albumId = req.params.id;

    Album.findByIdAndRemove(albumId, (err, albumRemoved) => {
        if(err){
            res.status(500).send({menssage: 'Error al eliminar el album(s)'});
        }else{
            if(!albumRemoved){
                res.status(404).send({menssage: 'El album asociado al artista no ha sido eliminado'});
            }else{
                //BUscamos y eliminamos las canciones asociadas al album
                Song.find({album: albumRemoved._id}).remove((err, songRemoved) => {
                    if(err){
                        res.status(500).send({menssage: 'Error al eliminar cancion(es)'});
                    }else{
                        if(!songRemoved){
                            res.status(404).send({menssage: 'Cancion(es) asociado al album no ha(n) sido eliminada(s)'});
                        }else{
                            res.status(200).send({album: albumRemoved});
                        }
                    }
                });
            }
        }
    });
}

function uploadImage(req, res){
    var albumId = req.params.id;
    var file_name = "no hay imagen...";

    if(req.files){
        var file_path = req.files.image.path;
        var file_split = file_path.split('/');
        var file_name = file_split[2];
        
        
        var ext_split = file_name.split('.');
        var file_ext = ext_split[1];

        if(file_ext == 'png' || file_ext == 'jpeg' || file_ext == 'gif'){
            Album.findByIdAndUpdate(albumId, {image: file_name}, (err, albumUpdated) => {
                    if(!albumUpdated){
                        res.status(404).send({menssage: 'NO se ha podido actualizar el album'});
                    }else{
                        res.status(200).send({artist: albumUpdated});
                    }
            });
        }else{
            res.status(200).send({menssage: 'La extensión no es válida'});
        }
    }else{
        res.status(200).send({menssage: 'NO haz subido ninguna imagen'});
    }
}

function getImageFile(req, res){

    // se obtiene el path de la imagen que viene por la url
    var imageFile = req.params.imageFile;
    //se busca la imagen en la carpeta donde de imagen de usuarios (disco duro)
    var path_file = './uploads/albums/'+imageFile;
    //se comprueba la existencia de la imagen
    fs.exists(path_file, function(exists){
        if(exists){
            //si existe, se devuelve el path y se resuelve para mostrar la imagen en pantalla
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({menssage: 'NO existe la imagen...'})
        }

    });
}

module.exports = {
    getAlbum,
    saveAlbum,
    getAlbums,
    updateAlbum,
    deleteAlbum,
    uploadImage,
    getImageFile
}