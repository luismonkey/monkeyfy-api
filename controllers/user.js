'use strict'

var bcrypt = require('bcryptjs'); 
var User = require('../models/user');
var jwt = require('../services/jwt');

var fs = require('fs');
var path = require('path');

function prueba(req, res){
    res.status(200).send({
        menssage: 'Acción de user'
    });
}

function saveUser(req, res){
    var user = User();

    var params = req.body;

    user.name = params.name;
    user.surname = params.surname;
    user.email = params.email;
    user.role = 'ROLE_ADMIN';
    user.image = 'null';

    if(params.password){
        //encriptar contraseña
        bcrypt.hash(params.password, 9, function(err, hash){
            user.password = hash;

            if(user.name != null && user.surname != null && user.email != null){
                //GUardar los datos
                user.save((err, userStored) => {
                    if(err){
                        res.status(500).send({menssage: 'error al guardar el usuario'});
                    }else{
                        if(!userStored){
                            res.status(404).send({menssage: 'no se ha registrado el usuario'});
                        }else{
                            res.status(200).send({user: userStored});
                        }
                    }
                });
            }else{
                res.status(200).send({menssage: 'por favor, completa todos los datos'});
            }

        });
    }else{
        res.status(200).send({menssage: 'por favor, introduzca contraseña'});
    }

}

function loginUser(req, res){
    var params = req.body;
    
    var email = params.email;
    var password = params.password;
    

    User.findOne({email: email.toLowerCase()}, (err, user) => {
        
        if(err){
            res.status(500).send({menssage: 'Error en petición'});
        }else{
            if(!user){
                res.status(404).send({menssage: 'El usuario no existe'});
            }else{
                //Comprobar la contraseña
                bcrypt.compare(password, user.password, function(err, check){
                    
                    if(check){
                        //devolver los datos del usuario logueado
                        if(params.gethash){
                            //devolver token jwt
                            res.status(200).send({
                                token: jwt.createToken(user)
                            });
                        }else{
                            res.status(200).send({user});
                        }
                    }else{
                        res.status(404).send({menssage: 'el usuario no ha podido loguearse'});
                    }
                });
            }
        }
    });
}

function updateUser(req, res){
    //recogemos el id que debe venir por la url
    var userId = req.params.id;

    //recogemos todos los datos que nos vienen por post (el cuerpo), los datos a actualizar
    var update = req.body;

    User.findByIdAndUpdate(userId, update, (err, userUpdated) => {
        if(err){
            res.status(500).send({menssage: 'Error al actualizar el usuario'});
        }else{
            if(!userUpdated){
                res.status(404).send({menssage: 'NO se ha podido actualizar el usuario'});
            }else{
                res.status(200).send({user: userUpdated});
            }
        }
    });
}

//metodo para subir imagen

function uploadImage(req, res){
    var userId = req.params.id;
    var file_name = "no hay imagen...";

    if(req.files){
        var file_path = req.files.image.path;
        var file_split = file_path.split('/');
        var file_name = file_split[2];
        
        
        var ext_split = file_name.split('.');
        var file_ext = ext_split[1];

        if(file_ext == 'png' || file_ext == 'jpeg' || file_ext == 'gif'){
            User.findByIdAndUpdate(userId, {image: file_name}, (err, userUpdated) => {
                    if(!userUpdated){
                        res.status(404).send({menssage: 'NO se ha podido actualizar el usuario'});
                    }else{
                        res.status(200).send({user: userUpdated});
                    }
            });
        }else{
            res.status(200).send({menssage: 'La extensión no es válida'});
        }
    }else{
        res.status(200).send({menssage: 'NO haz subido ninguna imagen'});
    }
}

function getImageFile(req, res){

    // se obtiene el path de la imagen que viene por la url
    var imageFile = req.params.imageFile;
    //se busca la imagen en la carpeta donde de imagen de usuarios (disco duro)
    var path_file = './uploads/users/'+imageFile;
    //se comprueba la existencia de la imagen
    fs.exists(path_file, function(exists){
        if(exists){
            //si existe, se devuelve el path y se resuelve para mostrar la imagen en pantalla
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({menssage: 'NO existe la imagen...'})
        }

    });
}

module.exports = {
    prueba,
    saveUser,
    loginUser,
    updateUser,
    uploadImage,
    getImageFile
};