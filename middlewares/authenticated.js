'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'key_secret';

exports.ensureAuth = function(req, res, next){
    if(!req.headers.authorization){
        return res.status(403).send({menssage: 'La petición no tiene cabecero de autenticación'});
    }
    //por lo generarl el token viene con comillas por delante y por detrás
    //con el metodo replace le decimos que las replace por nada. 
    var token = req.headers.authorization.replace(/['"]+/g, '');

    //decodificamos el token

    try{
        var payload = jwt.decode(token, secret);

        if(payload.exp <= moment().unix()){
            res.status(401).send({menssage: 'El token ha expirado'});
        }
    }catch(ex){
        //console.log(ex);
        res.status(404).send({menssage: 'Token no valido'});
    }

    req.user = payload;

    next();

};